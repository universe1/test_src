#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <alsa/asoundlib.h>

#include <time.h>
#include <sys/time.h>

int init_alsa_sound_playback(snd_pcm_t **pcm, snd_pcm_format_t format, int rate, int channels)
{
    int ret;
    int dir = 0;
    snd_pcm_uframes_t frames;
    snd_pcm_uframes_t periodsize;
    snd_pcm_hw_params_t *hw_params = NULL; //硬件信息和PCM流配置

    //1. 打开PCM，最后一个参数为0意味着标准配置
    ret = snd_pcm_open(pcm, "default", SND_PCM_STREAM_PLAYBACK, 0);
    if (ret < 0)
    {
        perror("snd_pcm_open");
        return -1;
    }

    //2. 分配snd_pcm_hw_params_t结构体
    ret = snd_pcm_hw_params_malloc(&hw_params);
    if (ret < 0)
    {
        perror("snd_pcm_hw_params_malloc");
        goto init_alsa_sound_playback_error;
    }

    //3. 初始化hw_params
    ret = snd_pcm_hw_params_any(*pcm, hw_params);
    if (ret < 0)
    {
        perror("snd_pcm_hw_params_any");
        goto init_alsa_sound_playback_error;
    }

    //4. 初始化访问权限
    ret = snd_pcm_hw_params_set_access(*pcm, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED);
    if (ret < 0)
    {
        perror("snd_pcm_hw_params_set_access");
        goto init_alsa_sound_playback_error;
    }

    //5. 初始化采样格式
    ret = snd_pcm_hw_params_set_format(*pcm, hw_params, format);
    if (ret < 0)
    {
        perror("snd_pcm_hw_params_set_format");
        goto init_alsa_sound_playback_error;
    }

    //6. 设置采样率，如果硬件不支持我们设置的采样率，将使用最接近的
    ret = snd_pcm_hw_params_set_rate_near(*pcm, hw_params, &rate, &dir);
    if (ret < 0)
    {
        perror("snd_pcm_hw_params_set_rate_near");
        goto init_alsa_sound_playback_error;
    }

    //7. 设置通道数量
    ret = snd_pcm_hw_params_set_channels(*pcm, hw_params, channels);
    if (ret < 0)
    {
        perror("snd_pcm_hw_params_set_channels");
        goto init_alsa_sound_playback_error;
    }

    //8. 设置hw_params
    ret = snd_pcm_hw_params(*pcm, hw_params);
    if (ret < 0)
    {
        perror("snd_pcm_hw_params");
        goto init_alsa_sound_playback_error;
    }

    snd_pcm_hw_params_free(hw_params);

    return 0;

init_alsa_sound_playback_error:
    if(hw_params != NULL)
        snd_pcm_hw_params_free(hw_params);
    snd_pcm_close(*pcm);
    return -1;
}

int alsa_play_sound(snd_pcm_t *pcm, const void *buffer, snd_pcm_uframes_t size)
{
    int ret;

    //写音频数据到PCM设备
    ret = snd_pcm_writei(pcm, buffer, size);
    if (ret == -EPIPE)
    {
        /* EPIPE means underrun */
        fprintf(stderr, "underrun occurred\n");
        //完成硬件参数设置，使设备准备好
        snd_pcm_prepare(pcm);
        return 0;
    }
    else if (ret < 0)
    {
        fprintf(stderr, "error from writei: %s\n", snd_strerror(ret));
        return -1;
    }

    return 0;
}

int main(int argc, char *argv[])
{
    int ret;
    unsigned int val;
    int dir = 0;
    char *buffer;
    int size;
    snd_pcm_uframes_t frames;
    snd_pcm_uframes_t periodsize;
    snd_pcm_t *playback_handle; //PCM设备句柄pcm.h

    struct timeval tv;

    if (argc != 2)
    {
        printf("error: alsa_play_test [music name]\n");
        exit(1);
    }
    printf("play song %s by wolf\n", argv[1]);
    FILE *fp = fopen(argv[1], "rb");
    if (fp == NULL)
        return 0;
    if(fseek(fp, 44, SEEK_SET) != 0)
    {
        fprintf(stderr, "fseek error:%s", strerror(errno));
        exit(1);
    }

    ret = init_alsa_sound_playback(&playback_handle, SND_PCM_FORMAT_S16_LE, 16000/*44100*/, 2);//2
    if(ret < 0)
    {
        exit(1);
    }

    /* Set period size to 32 frames. */
//    frames = 480;
//    periodsize = frames * 2;
//    ret = snd_pcm_hw_params_set_buffer_size_near(playback_handle, hw_params, &periodsize);
//    if (ret < 0)
//    {
//        printf("Unable to set buffer size %li : %s\n", frames * 2, snd_strerror(ret));
//
//    }
//    periodsize /= 2;
//
//    ret = snd_pcm_hw_params_set_period_size_near(playback_handle, hw_params, &periodsize, 0);
//    if (ret < 0)
//    {
//        printf("Unable to set period size %li : %s\n", periodsize, snd_strerror(ret));
//    }

    /* Use a buffer large enough to hold one period */
//    snd_pcm_hw_params_get_period_size(hw_params, &frames, &dir);

    frames = 960;//480
    size = frames * 2 * 2/*2*/; /* 2 bytes/sample, 2 channels */
    buffer = (char *) malloc(size);
    printf("size = %d\n", size);

    snd_pcm_prepare(playback_handle);
    while (1)
    {
        ret = fread(buffer, 1, size, fp);
        if (ret == 0)
        {
            fprintf(stderr, "end of file on input\n");
            break;
        }
        else if (ret != 1)
        {
            printf("unexcept read size:%d, except %d\n", ret, size);
        }

        //9. 写音频数据到PCM设备
        while ((ret = alsa_play_sound(playback_handle, buffer, frames)) < 0)
        {
//            printf("sleep\n");
//            usleep(300000);
        }

        gettimeofday(&tv, NULL);
        printf("%d.%d\n", (int)tv.tv_sec, (int)tv.tv_usec);

//        usleep(1000);
    }

    //10. 关闭PCM设备句柄
    snd_pcm_close(playback_handle);
    free(buffer);
    fclose(fp);

    return 0;
}

//注意：编译的时候应该保持“gcc -o test test.c -L. -lasound”的格式，运行的时候应该保持"./test clip2.wav"这种格式。
