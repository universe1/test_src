#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "list.h"

pthread_t _audio_push_thread_id;
pthread_t _audio_sound_thread_id;
static pthread_mutex_t audio_record_event_list_mtx = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t audio_record_event_list_mtx1 = PTHREAD_MUTEX_INITIALIZER;

typedef struct recv_audio_node_t
{
    struct list_head list;
    char *buff;
    int len;
}recv_audio_node_t, *p_recv_audio_node_t;

static LIST_HEAD(audio_event_list);

int vd_reset_audio_node1(char *buffer, int len)
{
	recv_audio_node_t *new_node = NULL;
    char *data_copy = NULL;

    if(len > 0)
    {
        data_copy = (char *)malloc(len);
        if(NULL == data_copy)
        {
        	printf("%s:malloc error\n", __FUNCTION__);
            return -1;
        }
        memcpy(data_copy, buffer, len);
    }

    new_node = (recv_audio_node_t *)calloc(1, sizeof(recv_audio_node_t));
    new_node->buff = data_copy;
    new_node->len = len;
    INIT_LIST_HEAD(&new_node->list);

    if (pthread_mutex_lock(&audio_record_event_list_mtx) != 0)
    {
    	printf("%s:pthread lock main_thread_event_list_mtx error\n", __FUNCTION__);
        if(new_node->buff != NULL)
            free(new_node->buff);
        free(new_node);
        return -1;
    }
    list_add_tail(&new_node->list, &audio_event_list);
    if (pthread_mutex_unlock(&audio_record_event_list_mtx) != 0)
    {
    	printf("%s:pthread unlock main_thread_event_list_mtx error\n", __FUNCTION__);
        /*节点已经添加，认为执行成功*/
    }

	return 0;
}

int vd_read_audio_node1(char *buf)
{
	recv_audio_node_t *pos = NULL;
	struct list_head  *lilen = NULL;
	int len;
	int list_len;



	if(audio_event_list.next == &audio_event_list)
	{
		printf("audio_event_list list is not data");
		return 0;
	}

    if (pthread_mutex_lock(&audio_record_event_list_mtx) != 0)
    {
    	printf("%s:pthread lock main_thread_event_list_mtx error\n", __FUNCTION__);
        return -1;
    }

	lilen = audio_event_list.prev;
	for(list_len = 0; lilen->prev != audio_event_list.prev;list_len++)
	{
		lilen = lilen->prev;
	}
	printf("==>list len:%d\n", list_len);

	pos = list_entry(audio_event_list.next, recv_audio_node_t, list);
	len = pos->len;
	memcpy(buf, pos->buff, len);
	if (pos->buff != NULL)
	{
		free(pos->buff);
		pos->buff = NULL;
	}

	list_del(&pos->list);
	free(pos);
    if (pthread_mutex_unlock(&audio_record_event_list_mtx) != 0)
    {
    	printf("%s:pthread unlock main_thread_event_list_mtx error\n", __FUNCTION__);
        /*节点已经添加，认为执行成功*/
    }
	return len;
}

void *_thread_audio_push(void *unused)
{
		char buf[100] = {0};
		int mac = 1;
		int mad = 0;
	while (1)
	{
		for (mad = 0; mad < 20; mad++)
		{
			mac++;
			sprintf(buf, "const :%d\n", mac);
			vd_reset_audio_node1(buf, 10);
			printf("push\n");
			usleep(100000);
		}
		sleep(30);

	}
}

void *_thread_audio_sound(void *unused)
{
	int  recv_len = 0;
	char buff[1000]={0};
	int list_len = 0;

	while(1)
	{



		recv_len = vd_read_audio_node1(buff);
		if(recv_len >= 0)
		{
			printf("%d\n",recv_len);
			printf("buff:%s\n",buff);
			sleep(1);
			continue;
		}
		printf("recv < 0 buffer %s\n",buff);
		sleep(1);
	}

	vd_sound_pcm_close_event();
}

int vd_audio_init_record_thread(void)
{
	if(pthread_create(&_audio_push_thread_id, NULL, _thread_audio_push, NULL) != 0)
	{
		printf("create video record failed\n");
		return -1;
	}

	if(pthread_create(&_audio_sound_thread_id, NULL, _thread_audio_sound, NULL) != 0)
	{
		printf("create video record failed\n");
		return -1;
	}

	return 0;
}

void main(void)
{
	vd_audio_init_record_thread();
	while(1)
	{
		sleep(1);
	}

}






























