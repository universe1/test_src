#include <pthread.h>
#include <stdio.h>

static pthread_mutex_t vd_record_run_flag_mtx = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t vd_record_run_flag_ttl = PTHREAD_MUTEX_INITIALIZER;

void pthread_nutex_loclk_test_run(int temp)
{
	int i, j, c;
	int cc = 300500;

	if(pthread_mutex_lock(&vd_record_run_flag_mtx) != 0)
	{
		printf("pthread lock vd_record_run_flag_mtx error");
	    return;
	}
	if(temp)
	for(i = 0; i <100; i++)
		{
			printf("%2d ", i);
			for(j = 0; j < cc; j++)
			{
				c = j;
			}
			if((i%30) == 0)
				printf("\n");
		}
	else
		for(i = 0; i <100; i++)
			{
				printf("%2da ", i);
				for(j = 0; j < cc; j++)
				{
					c = j;
				}
				if((i%30) == 0)
					printf("\n");
			}


	printf("\n");
    if(pthread_mutex_unlock(&vd_record_run_flag_mtx) != 0)
    {
    	printf("pthread unlock vd_record_run_flag_mtx error");
        return;
    }

}

void *vd_thread_run_cont_event(void *unused)
{
	int j, c, cc =1000000;

	while(1)
	{
		pthread_nutex_loclk_test_run(0);
		usleep(100000);
	}
	return NULL;
}

int run_cont_event_init_thread(void)
{
    pthread_t thread_id;

    if(pthread_create(&thread_id, NULL, &vd_thread_run_cont_event, NULL) != 0)
    {
    	printf("create thread read key error");
        return -1;
    }

    return 0;
}

void *vd_thread_run_letter_event(void *unused)
{
	int j, c, cc =1000000;
	while(1)
	{
		pthread_nutex_loclk_test_run(1);
		usleep(10);
	}
	return NULL;
}

int run_letter_event_init_thread(void)
{
    pthread_t thread_id;

    if(pthread_create(&thread_id, NULL, &vd_thread_run_letter_event, NULL) != 0)
    {
    	printf("create thread read key error");
        return -1;
    }

    return 0;
}

int main()
{
	run_cont_event_init_thread();
	run_letter_event_init_thread();

	while(1)
	{
		sleep(1);
	}
}

